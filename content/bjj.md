+++
aliases = ["posts", "articles", "blog", "showcase", "docs"]
title = "Brazilian Jiu Jitsu"
author = "Austin"
tags = ["services", "bjj", "martial arts", "brazilian jiu jitsu"]
+++

{{< image-bjj-page >}}

Submission grappling is one of the world's premiere martial arts.  The variety known as Brazilian Jiu Jtsu (BJJ) became particularly well know because of the Gracie family and the UFC in the early 1990s.

I began training mixed martial arts (MMA) in 2010.  In 2012 I moved to Georgia, USA and in 2013 took a long hiatus from martial arts to pursue outdoor rock climbing.  In 2019 I returned to the sport and hit the ground running, earning my black belt from Ranieri Paiva and Kyle Cammarano in June 2023.

I have competed in a variety of settings including boxing, MMA, and BJJ.  I have also worked with numerous local law enforcement agencies regarding defensive tactics to mitigate escalations in use of force.

In terms of "experiential stress management", nothing compares to grappling.  BJJ is an excellent opportunity to explore the many dimensions of your personality and physicality, including those you may not like looking at or even acknowledging, as well as the opportunity to grow and nurture those aspects.

> I currently offer private (1:1) and semi-private (1:2) lessons in Gi (with kimono) and NoGi (without Kimono) BJJ as well as self-defense, taught at [[Integirty BJJ and Fitness]](https://maps.app.goo.gl/k3o9LXpkmcKemRXeA) in Carrollton, GA.

---

{{< cta-bjj >}}
