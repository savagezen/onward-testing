+++
aliases = ["posts", "articles", "blog", "showcase", "docs"]
title = "Counseling"
author = "Austin"
tags = ["services", "counseling"]
+++

{{< image-counseling-page >}}

I began practicing counseling as an intern at the University of West Georgia in 2014.  I graduated with my Master's Degree  in Psychology in 2015 and was also honored with the [[Mike Arons Humanistic Psychology Scholarship]](https://www.westga.edu/academics/art-culture-science/anthro-psych-soc/psychology/scholarships-awards.php#d19e305).  Since then I have worked in every level of care in the mental health system including: community-based, school-based, IFI, IOP / PHP, inpatient intake, private practice, and university counseling.

After a decade of practice and growing frustration with corporate compromises in ethics and standards of care I opened [[Onward Counseling & Training]](/) in May of 2024 with the goals of (1) maintaining *"excellence as the standard"* and (2) combining clinical mental health services and physical training to address the body, mind, and spirit as *one piece.*

In addition to being a Licensed Professional Counselor (LPC), I am also a Certified Professional Counselor Supervisor (CPCS) in the State of Georgia and a clinical member of the Licensed Professional Counselor's Association of Georgia (LPCAGA).

*From [[Psychology Today]](https://www.psychologytoday.com/profile/308634)*:

*I work with a variety of clients on an integrative and holistic wellness basis.  Currently, I specialize in working with competitive athletes and first responders; emphasizing body-centered interventions including fitness, breathwork, and nutrition.  Before returning to private practice, I pioneered unique endeavors at the University of West Georgia as liaison between counseling, integrative wellness, and Division 1 athletics.  Previously, I have focused on adult and adolescent personality disorders, and the utilization of psychodynamic, person-centered, mentalization-based, DBT, and play therapy modalities.*

*I believe that psychotherapy is a fundamentally relational process.  With adolescents and adults I emphasize developing mood regulation, distress tolerance, interpersonal relationships, and self-efficacy.  With athletes and other performance-driven individuals I focus on the complexities of training, integration, and personal identity.*

*I believe my role as a therapist is similar to both a detective and a tour guide in helping you navigate and explore who you are, where you've been, who you want to become, and where you want to go.  I am also a personal trainer, certified nutrition coach, and Brazilian Jiu Jitsu black belt, providing separate services in those areas as well.*

I currently offer:

- Virtual / telehealth counseling adults in the State of Georgia.
- Clinical supervision and consultation to APCs and LPCs.
- Synchronous and asynchronus [[continuing education workshops]](https://onward.thinkific.com).

---

{{< cta-counseling >}}
