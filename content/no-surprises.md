+++
aliases = ["posts", "articles", "blog", "showcase", "docs"]
title = "The 'No Surprises Act' & Good Faith Estimates"
author = "Austin"
tags = ["services", "counseling", "no surprises act", "good faith estimates"]
+++

The No Surprises Act was passed in December 2020, under Section 2799B-6 of the Public Health Act, with the aim of protecting consumers from receiving unexpected medical bills.  

The Good Faith Estimate provision of the No Surprises Act federally mandates that healthcare providers must give clients an estimate of anticipated healthcare items and services, using what is called a “Good Faith Estimate.” This took effect on January 1, 2020.

**What is a Good Faith Estimate?**

A Good Faith Estimate is an estimate of the total expected costs of non-emergency healthcare items or services.

* Intends to offer predictability & transparency in how much clients will be charged for healthcare services prior to their appointment. 
* Includes all regularly scheduled appointments (i.e. therapy sessions).
* Does NOT include no-shows, late cancellations, or other services related to crisis care, which by definition are unexpected and cannot be predicted for the purpose of compiling a Good Faith Estimate in advance. 
* May also include consultations with client collateral contacts, fees related to paperwork requests, and other legal administrative fees related to client care, when such items are scheduled in advance. 

In my practice, I offer Good Faith Estimates that project for the identified treatment duration on each client's treatment plan; typically 3 - 6 months or up to 12 months in advance.  Each time a treatment plan is updated the Good Faith Estimate is subject to change; however, the idea is to give you a reasonable idea what to expect in terms of therapy costs for what we agree to work on together based on my current rate that we agree upon at the time a treatment plan is made or updated.

**What are your rights as a client?**

 I support each and every client knowing their rights as it pertains to the No Surprises Act. The Good Faith Estimate offers specific protections: 

* You have a right to receive a Good Faith Estimate even if you get a superbill from me for out-of-network insurance.
* You have the right to receive a Good Faith Estimate for the total expected cost of any non-emergency healthcare service or items.
* You have the right to receive your Good Faith Estimate in writing within at least 1 business day before your scheduled healthcare service or item.  If a service is scheduled at least 10 business days in advance, the Good Faith Estimate must be provided within 3 business days (of the scheduling, not the appointment itself).  If a service is scheduled at least 3 business days in advance, the Good Faith Estimate must be provided within 1 business day of scheduling.
* You have the right to request a Good Faith Estimate before you schedule a healthcare service or item.  For services scheduled less than 3 business days in advance, please not that a Good Faith Estimate is not required by federal law, and will not be provided for you in written form except by request.
* You have the right to receive a requested Good Faith Estimate within 3 business days.
* You have the right to dispute a bill that exceeds your Good Faith Estimate.  The federal government offers a dispute resolution process for this purpose.

**Make sure to save a copy or picture of your Good Faith Estimate.**  For questions about the dispute process or for more information about your right to a Good Faith Estimate, please visit [[www.cms.gov/nosurprises]](https://cms.gov/nosurprises).

---

{{< contact >}}
