+++
aliases = ["posts", "articles", "blog", "showcase", "docs"]
title = "Personal Training & Fitness"
author = "Austin"
tags = ["services", "fitness"]
+++

{{< image-fitness-page >}}

Whether you're a competitive athlete, trying to get back into shape, or have never been in shape, my goal is to walk with clients on an exploratory process that utilizes the body and physical movements to explore overlapping sensations, emotions, drives, desires, identities, and barriers in our psychological and physiological sates.  Some call this "sport psychology", I call it being human.

I use a creative blend of skills accumulated from studying with groups like [[Ollin]](https://www.weareollin.com/) and [[Shift Adapt]](https://shiftadapt.com) to cultivate autonomous athletes who are not only physically fit, but fit to oversee their own training and programming.  A variety of tools can be used, but the global objective is improved insight and awareness as to methods of stimulation (development towards a goal), the sensations associated with those methods, and the recovery cost those methods demand.

This process is as beneficial for the Division 1 competitor as it is for the first responder or for the average person looking to improve their physical prowess, mental attunement, and quality of life.

> I currently offer private individual fitness and nutrition training.  For group or team related events, please see the [[Seminars and Special Events]](seminars) page.

---

{{< cta-fitness >}}
