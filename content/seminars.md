+++
aliases = ["posts", "articles", "blog", "showcase", "docs"]
title = "Seminars & Special Events"
author = "Austin"
tags = ["services", "seminars", "special events", "self defense", "breath"]
+++

{{< image-seminars-page >}}

I have worked with many different teams and groups ranging from defensive tactics instruction with local law enforcement to personal preparedness and protection seminars with private businesses and citizens.  I have also facilitated special events such as being a panelist for men's mental health issues and exploring distress tolerance and experiential stress management through breath work in DBT (dialectical behavioral therapy) groups.

Additionally, I have worked with non-clinical staff (e.g. coaches, trainers, teachers, etc.) regarding ways to support athletes and young adults who may be struggling with their mental health -- including the development of a university-wide crisis protocol at the University of West Georgia.

I am available for private group events for [[Brazilian Jiu Jitsu]](bjj), Breath Work for Clinical Mental Health (CEU), team or individual sport / performance psychology consultation and speaking events, Breath Work as Experiential Stress Management (workshop), and Personal Preparedness and Protection (seminar).

> Please contact me below for rates, scheduling, and opportunities!

---

{{< contact >}}
