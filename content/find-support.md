+++
aliases = ["posts", "articles", "blog", "showcase", "docs"]
title = "Need to talk to someone urgently?"
author = "Austin"
tags = ["services", "counseling"]
+++

***Life Threatening Emergency:*** [911](tel:911)

**Georgia Crisis & Access Hotline:** [[1-800-715-4225]](tel:+18007154225)

**Suicide & Crisis Lifeline:** call or text [[988]](tel:988)

**Georgia Crisis Text Line:** text "HOME" to [[741-741]](tel:741741)

---

**Veterans Crisis Hotline:** [[1-800-273-8255]](tel:+18002738255)

**Substance Abuse Hotline:** [[1-800-622-4357]](tel:+18006224357)

**National Sexual Assault Hotline:** [[1-800-656-4673]](tel:+18006564673)

**National Youth Crisis Hotline:** [[1-800-422-4673]](tel:+18004224673)

**National Human Trafficking Hotline:** [[1-800-373-7888]](tel:+18003737888)

**Georgia Coalition Against Domestic Violence:** [[1-800-334-2836]](tel:+18003342836)

---

{{< contact >}}
